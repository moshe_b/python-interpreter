#pragma once
#include "type.h"
class Boolean :
    public Type
{
public:
    Boolean(bool val);
    bool isPrintable() const;
    std::string toString() const;
    std::string getType() const;
    bool is_true() const;
private:
    bool _val;
};

