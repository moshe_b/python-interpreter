#ifndef TYPE_H
#define TYPE_H
#include <string>


class Type
{
public:
	Type(bool tempVal);
	virtual bool isPrintable() const = 0;
	virtual std::string toString() const = 0;
	virtual std::string getType() const = 0;
	virtual bool is_true() const = 0;
	bool isTemp();
	void setIsTemp(bool tempVal);
private:
	bool _isTemp;
};





#endif //TYPE_H
