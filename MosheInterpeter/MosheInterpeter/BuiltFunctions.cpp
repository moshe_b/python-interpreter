#include "BuiltFunctions.h"

std::map<std::string, std::pair<builtInFunction, int>>  BuiltFunctions::_functions;

void BuiltFunctions::initFunctions()
{
    _functions["type"] = std::make_pair<builtInFunction, int>((builtInFunction)type, 1);
    _functions["len"] = std::make_pair<builtInFunction, int>((builtInFunction)len, 1);
    _functions["abs"] = std::make_pair<builtInFunction, int>((builtInFunction)abs, 1);
    _functions["all"] = std::make_pair<builtInFunction, int>((builtInFunction)all, 1);
}

Type* BuiltFunctions::executeFunction(std::string nameFunction, List* l_arguments)
{
    return _functions[nameFunction].first(0, l_arguments);
}

bool BuiltFunctions::isFuncExist(std::string nameFunc)
{
    return _functions.find(nameFunc) != _functions.end();
}

Type* BuiltFunctions::type(int numberArgs, List* l_arguments)
{
	Type* obj = &(*l_arguments)[0];
    std::string typeStr = "\'type<  \'" + obj->getType() + "\'>\'";
	return new String(typeStr);
}

Type* BuiltFunctions::len(int numberArgs, List* l_arguments)
{
    if (String* str = dynamic_cast<String*>(&(*l_arguments)[0]))
    {
        //remove quotation marks from str
        return new Integer(str->toString().size()-2);
    }
    else if (List* list = dynamic_cast<List*>(&(*l_arguments)[0]))
    {
        return new Integer(list->getList().size());
    }

    throw SyntaxException();
}

Type* BuiltFunctions::abs(int numArgs, List* arguments)
{
   if (Integer* val = dynamic_cast<Integer*>(&(*arguments)[0]))
   {
        return *val < 0 ? new Integer(Integer(- 1) * (*val)) : val;
   }
    
   return nullptr;
}

Type* BuiltFunctions::all(int numArgs, List* arguments)
{
    
    if (arguments->getList().size() != 1)
        throw SyntaxException();

    if (List* m_list = dynamic_cast<List*>(&(*arguments)[0]))
    {
        std::vector<Type*> objects = m_list->getList();
        for (int i = 0; i < objects.size(); i++)
        {
            if (!objects[i]->is_true())
                return new Boolean(false);
        }
    }

    return new Boolean(true);
}
