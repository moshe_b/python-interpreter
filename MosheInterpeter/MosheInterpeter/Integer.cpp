#include "Integer.h"

Integer::Integer(int val) : Type(false)
{
	this->_val = val;
}

Integer::Integer(const Integer& integer) : Type(false)
{
	_val = integer._val;
}

bool Integer::isPrintable() const
{
	return true;
}

std::string Integer::toString() const
{
	return std::to_string(_val);
}

std::string Integer::getType() const
{
	return "int";
}

bool Integer::is_true() const
{
	return _val != 0;
}

bool Integer::operator<(Integer const& obj)
{
	return _val < obj._val;
}

bool Integer::operator>(Integer const& obj)
{
	return _val > obj._val;
}

bool Integer::operator==(Integer const& obj)
{
	return _val == obj._val;
}

Integer Integer::operator*(Integer const& obj)
{
	Integer value(obj._val * _val);
	return value;
}
