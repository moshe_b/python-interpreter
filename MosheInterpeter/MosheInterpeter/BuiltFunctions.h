#pragma once

#include "Helper.h"
#include <string>
#include <map>
#include <stdarg.h>
#include "List.h"

typedef Type* (*builtInFunction)(int, List*);

class BuiltFunctions
{
public:
	static void initFunctions();
	static Type* executeFunction(std::string nameFunction, List* l_arguments);
	static bool isFuncExist(std::string nameFunc);
private:
	static Type* type(int numberArgs, List*);
	static Type* len(int numberArgs, List*);
	static Type* abs(int numArgs, List* arguments);
	static Type* all(int numArgs, List* arguments);
	static std::map<std::string, std::pair<builtInFunction, int>> _functions;
};

