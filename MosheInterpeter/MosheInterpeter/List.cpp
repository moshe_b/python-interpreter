#include "List.h"

List::List(std::string listStr) : Sequence()
{

}

bool List::isPrintable() const
{
    return true;
}

std::string List::toString() const
{
    std::string listStr;

    listStr = "[";
    for (int i = 0; i < _list.size()-1; i++)
    {
        listStr += _list[i]->toString()+", ";
    }

    listStr += _list[_list.size() - 1]->toString() + "]";

    return listStr;
}

std::vector<Type*>& List::getList()
{
    return _list;
}

std::string List::getType() const
{
    return "list";
}

Type& List::operator[](int index)
{
    if (index >= 0 && index < _list.size())
        return *this->_list[index];
    //need to throw index error
}

bool List::is_true() const
{
    return this->_list.size() != 0;
}
