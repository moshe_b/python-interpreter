#include "Boolean.h"

Boolean::Boolean(bool val) : Type(false)
{
    _val = val;
}

bool Boolean::isPrintable() const
{
    return true;
}

std::string Boolean::toString() const
{
    return _val ? "True" : "False";
}

std::string Boolean::getType() const
{
    return "bool";
}

bool Boolean::is_true() const
{
    return _val;
}
