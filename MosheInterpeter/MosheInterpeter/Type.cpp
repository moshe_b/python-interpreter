#include "type.h"

Type::Type(bool tempVal)
{
    _isTemp = tempVal;
}

bool Type::isTemp()
{
    return _isTemp;
}

void Type::setIsTemp(bool tempVal)
{
    _isTemp = tempVal;
}
