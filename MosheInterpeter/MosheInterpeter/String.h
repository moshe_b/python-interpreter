#pragma once
#include "Sequence.h"
#include "Helper.h"

class String :
    public Sequence
{
public:
    String(std::string val);
    bool isPrintable() const;
    std::string toString() const;
    std::string getType() const;
    bool is_true() const;
private:
    std::string _val;
};

