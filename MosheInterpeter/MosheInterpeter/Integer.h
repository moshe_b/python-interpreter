#pragma once
#include "type.h"
class Integer :
    public Type
{
public:
    Integer(int val);
    Integer(const Integer& integer);
    bool isPrintable() const;
    std::string toString() const;
    std::string getType() const;
    bool is_true() const;
    bool operator<(Integer const& obj);
    bool operator>(Integer const& obj);
    bool operator==(Integer const& obj);
    Integer operator*(Integer const& obj);
private:
    int _val;
};

