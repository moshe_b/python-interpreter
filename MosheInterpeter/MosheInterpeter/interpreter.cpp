#include "type.h"
#include "InterperterException.h"
#include "parser.h"
#include "BuiltFunctions.h"
#include <iostream>

#define WELCOME "Welcome to Magshimim Python Interperter version 1.0 by "
#define YOUR_NAME "Moshe Buznah"


int main(int argc,char **argv)
{
	std::cout << WELCOME << YOUR_NAME << std::endl;

	BuiltFunctions::initFunctions();
	std::string input_string;

	// get new command from user
	std::cout << ">>> ";
	std::getline(std::cin, input_string);
	
	while (input_string != "quit()")
	{
		try
		{
			// prasing command
			if (!Helper::isEmpty(input_string))
			{
				Type* obj = Parser::parseString(input_string);
				if (obj->isPrintable())
				{
					std::cout << obj->toString() << std::endl;
				}

				//delete obj; //clean heap memory
			}
		}
		catch (IndentationException& e)
		{
			std::cout << e.what() << std::endl;
		}
		catch (SyntaxException& e)
		{
			std::cout << e.what() << std::endl;
		}
		catch (NameErrorException& e)
		{
			std::cout << e.what() << std::endl;
		}
		// get new command from user
		std::cout << ">>> ";
		std::getline(std::cin, input_string);
	}

	Parser::freeMemory();
	return 0;
}


