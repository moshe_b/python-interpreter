#include "Void.h"

Void::Void() : Type(true)
{
}

bool Void::isPrintable() const
{
    return false;
}

std::string Void::toString() const
{
    return std::string();
}

std::string Void::getType() const
{
    return "void";
}

bool Void::is_true() const
{
    return false;
}
