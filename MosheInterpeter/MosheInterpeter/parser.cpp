#include "parser.h"
#include <iostream>

std::unordered_map<std::string, Type*> Parser::_variables;

Type* Parser::parseString(std::string str) throw()
{
	Type* obj = getVariableValue(str);
	if (obj != nullptr)
		return obj;


	if (makeAssignment(str))
	{
		return new Void();
	}

	if (isLegalVarName(str))
		throw NameErrorException(str);

	if (handleDel(str))
		return new Void();

	if (str[0] == ' ' || str[0] == '\t')
	{
		throw IndentationException();
	}
	else if (getType(str) != nullptr)
	{
		return getType(str);
	}

	Type* obj2 = handleBuiltInFunction(str);
	if (obj2)
	{
		return obj2;
	}

	throw SyntaxException();
}

Type* Parser::getType(std::string str)
{
	Helper::trim(str);

	if (Helper::isInteger(str))
	{
		return new Integer(std::stoi(str));
	}
	else if (Helper::isBoolean(str))
	{
		return new Boolean(str == "True" ? true : false);
	}
	else if (Helper::isString(str))
	{
		return new String(str);
	}
	
	List* lObj = new List("");
	if (Helper::setList(str, lObj))
	{
		return lObj;
	}
	
	if (Helper::isVarExist(str))
		return _variables[str];

	return nullptr;
}

bool Parser::isLegalVarName(std::string str)
{
	int i = 0;
	for (; i < str.size() && Helper::isDigit(str[0]) &&
		Helper::isUnderscore(str[i]) || Helper::isLetter(str[i]); i++);
	return i == str.size();
}

bool Parser::makeAssignment(std::string str)
{
	Helper::trim(str);
	// check '=' is in str
	int equalCounter = std::count(str.begin(), str.end(), '=');
	int equalIndex = str.find('=');
	if (equalCounter != 1 || equalIndex == 0 || equalIndex == str.size() - 1)
	{
		return false;
	}

	std::string variable = str.substr(0, equalIndex);
	std::string value = str.substr(equalIndex + 1, str.size());
	Helper::trim(variable);
	Helper::trim(value);

	Type* obj = getType(value);

	//check for ilegal variable name and that value is not valid type
	if (!isLegalVarName(variable))
		throw SyntaxException();
	/*else if (!isLegalVarName(variable))
	{
		throw SyntaxException();
	}*/
	
	

	if (_variables.find(value) != _variables.end()) //check if value is variable
	{
		if (List* l = dynamic_cast<List*>(_variables[value]))
		{
			//double point in lists
			_variables[variable] = _variables[value];
			return true;
		}

		delete _variables[variable];
		_variables[variable] = getType(_variables[value]->toString());
		return true;
	}
	else if (!obj && isLegalVarName(value)) //value is legal var name but is not exist
		throw NameErrorException(value);
	else if(!obj)
	{
		Type* funcObj = handleBuiltInFunction(value); 
		if (!funcObj)
			throw SyntaxException();
		else
			obj = funcObj;
	}

	_variables[variable] = obj;

	return true;
}

Type* Parser::getVariableValue(std::string str)
{
	if(_variables.find(str) == _variables.end())
		return nullptr;
	
	return _variables[str];
}

void Parser::freeMemory()
{
	std::unordered_map<std::string, Type*>::iterator itr = _variables.begin();
	for (itr; itr != _variables.end(); itr++)
	{
		delete itr->second;
	}
}

std::string Parser::getTypeOfVar(std::string str)
{
	std::string type = "";
	
	Helper::trim(str);
	int indexOpenBracket = str.find("(");
	int indexCloseBracket = str.find(")");

	if (indexCloseBracket != std::string::npos && 
		indexOpenBracket != std::string::npos && 
		str.substr(0, indexOpenBracket+1) == "type(" &&
		str.substr(indexCloseBracket, str.size()-1) == ")")
	{
		std::string val = str.substr(indexOpenBracket+1, indexCloseBracket-indexOpenBracket-1);
		Type* obj = getType(val);

		if (obj != nullptr)
			return "type<  \'"+obj->getType()+"\'>";

		if (_variables.find(val) != _variables.end())
		{
			return "type<  \'" + _variables.find(val)->second->getType() + "\'>";
		}

		throw NameErrorException(val); //no good
	}

	throw SyntaxException();
}

Type*  Parser::handleBuiltInFunction(std::string str)
{
	Helper::trim(str);
	int indexOpenBracket = str.find("(");
	std::reverse(str.begin(), str.end());
	int indexCloseBracket = str.size() - str.find(")");
	std::reverse(str.begin(), str.end());
	std::string nameFunc = str.substr(0, indexOpenBracket);
	std::string arguments = str.substr(indexOpenBracket+1, indexCloseBracket-indexOpenBracket-2);

	if (indexCloseBracket != std::string::npos &&
		indexOpenBracket != std::string::npos &&
		BuiltFunctions::isFuncExist(nameFunc))
	{
		List* l_arguments = new List("");

		if (Helper::setList("[" + arguments + "]", l_arguments))
		{
			return BuiltFunctions::executeFunction(nameFunc, l_arguments);
		}
		else
		{
			Type* type = handleBuiltInFunction(arguments); // another function maybe
			if (type != nullptr)
			{
				List* l_arguments2 = new List("");
				Helper::setList("[" + type->toString() + "]", l_arguments2);
				return BuiltFunctions::executeFunction(nameFunc, l_arguments2);
			}
		}
	}

	return nullptr;
}

std::unordered_map<std::string, Type*>& Parser::getVars()
{
	return _variables;
}

bool Parser::handleDel(std::string str)
{
	int indexDel = str.find("del");

	if (indexDel == 0 && str[3] == ' ')
	{
		std::string variable = str.substr(indexDel + 3, str.size());
		Helper::trim(variable);

		if(Helper::isVarExist(variable))
		{
			_variables.erase(variable);
			return true;
		}
		else
			throw NameErrorException(variable);
	
	}

	return false;
}


