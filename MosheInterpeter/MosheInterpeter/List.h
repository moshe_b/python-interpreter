#pragma once
#include "Sequence.h"
#include <vector>

class List :
    public Sequence
{
public:
    List(std::string listStr);
    bool isPrintable() const;
    std::string toString() const;
    std::vector<Type*>& getList();
    std::string getType() const;
    Type& operator[](int index);
    bool is_true() const;
private:
    std::vector<Type*> _list;
};

