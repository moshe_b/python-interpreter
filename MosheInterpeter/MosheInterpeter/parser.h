#ifndef PARSER_H
#define PARSER_H

#include "InterperterException.h"
#include "IndentationException.h"
#include "SyntaxException.h"
#include "NameErrorException.h"
#include "type.h"
#include "Helper.h"
#include "Integer.h"
#include "Boolean.h"
#include "String.h"
#include "Void.h"
#include "List.h"
#include "BuiltFunctions.h"
#include <string>
#include <unordered_map>
#include <iostream>
#include <sstream>
#include <iterator>


class Parser
{
public:
	static Type* parseString(std::string str) throw();
	static Type* getType(std::string);
	static bool isLegalVarName(std::string str);
	static bool makeAssignment(std::string str);
	static Type* getVariableValue(std::string str);
	static void freeMemory();

	//get the type of variable
	static std::string getTypeOfVar(std::string str);
	static Type* handleBuiltInFunction(std::string str);

	//gets
	static std::unordered_map<std::string, Type*>& getVars();

	static bool handleDel(std::string str);

private:
	static std::unordered_map<std::string, Type*> _variables;
};

#endif //PARSER_H
