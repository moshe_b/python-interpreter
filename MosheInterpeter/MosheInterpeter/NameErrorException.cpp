#include "NameErrorException.h"

NameErrorException::NameErrorException(std::string name)
{
    _name = name;
}

const char* NameErrorException::what() const throw()
{
    return (new std::string("NameError : name '"+_name+"' is not defined"))->c_str();
}
