#include "String.h"

String::String(std::string val) : Sequence()
{
    _val = val;
}

bool String::isPrintable() const
{
    return true;
}

std::string String::toString() const
{
    std::string retVal;
    
    /*if (_val.substr(1, _val.size() - 2).find("\'") != std::string::npos &&
        _val.substr(1, _val.size() - 2).find("\"") != std::string::npos)
    {
        return "\'\'\'" + _val.substr(1, _val.size() - 2) + "\'\'\'";
    }*/

    if (_val.substr(1, _val.size() - 2).find("\'") != std::string::npos)
    {
        return "\"" + _val.substr(1, _val.size()-2) + "\"";
    }
    else
        return "\'" + _val.substr(1, _val.size() - 2) + "\'";


    return _val;
}

std::string String::getType() const
{
    return "str";
}

bool String::is_true() const
{
    std::string val = _val;
    Helper::trim(val);
    return val != "\"\"";
}
