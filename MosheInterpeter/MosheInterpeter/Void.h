#pragma once
#include "type.h"
class Void :
    public Type
{
public:
    Void();
    bool isPrintable() const;
    std::string toString() const;
    std::string getType() const;
    bool is_true() const;
private:

};

