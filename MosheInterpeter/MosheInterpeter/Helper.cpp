#include "Helper.h"

bool Helper::isInteger(const std::string& s)
{
	int start = (s[0] == '-') ? 1 : 0;
	for (int i = start; i < s.size(); i++)
	{
		if (!isDigit(s[i]))
		{
			return false;
		}
	}

	return true && s != "";
}

bool Helper::isBoolean(const std::string& s)
{
	return (s == "True" || s == "False");
}


bool Helper::isString(const std::string& s)
{
	size_t end = s.size() - 1;

	if (s[0] == '\"' && end == s.find('\"', 1))
	{
		return true;
	}
	if (s[0] == '\'' && end == s.find('\'', 1))
	{
		return true;
	}

	return false;

}

bool Helper::isDigit(char c)
{
	return (c >= '0' && c <= '9');
}

void Helper::trim(std::string &str)
{
	rtrim(str);
	ltrim(str);

}

void Helper::rtrim(std::string &str)
{
	size_t endpos = str.find_last_not_of(" \t");
	if (std::string::npos != endpos)
	{
		str = str.substr(0, endpos + 1);
	}
}

void Helper::ltrim(std::string &str)
{
	size_t startpos = str.find_first_not_of(" \t");
	if (std::string::npos != startpos)
	{
		str = str.substr(startpos);
	}
}

void Helper::removeLeadingZeros(std::string &str)
{
	size_t startpos = str.find_first_not_of("0");
	if (std::string::npos != startpos)
	{
		str = str.substr(startpos);
	}
}

bool Helper::containApostrophe(std::string str)
{
	int i = 0;  
	for (i = 1; i < str.size()-1 && str[i] != '\''; i++);
	return i != str.size() - 1;
}

bool Helper::isEmpty(std::string str)
{
	int  i = 0;
	for (; i < str.size() && std::isspace(str[i]); i++);
	return i == str.size();
}

bool Helper::setList(std::string listStr, List* listObj)
{
	Helper::trim(listStr);

	int closeBracketCounter = std::count(listStr.begin(), listStr.end(), ']');
	int openBracketCounter = std::count(listStr.begin(), listStr.end(), '[');
	std::reverse(listStr.begin(), listStr.end());
	int closeBracketIndex = listStr.size() - listStr.find(']') - 1;
	std::reverse(listStr.begin(), listStr.end());
	int openBracketIndex = listStr.find('[');

	if (openBracketIndex != 0 || closeBracketIndex != listStr.size() - 1)
		return false;

	std::string list = listStr.substr(1, closeBracketIndex - openBracketIndex - 1);
	for(;list != "";)
	{
		Helper::trim(list);
		int openBracketIndex = list.find("[");
		if (openBracketIndex == 0)
		{
			List* l = new List("");
			std::string listParsing = getListFromStr(list);
			if (Helper::setList(listParsing, l))
			{
				listObj->getList().push_back(l);
				list = list.substr(list.find(listParsing) + listParsing.size(), list.size() - listParsing.size() - 1);
			}
			continue;
		}

		int commaIndex = list.find(",");
		if (commaIndex == std::string::npos)
		{
			Type* obj = Parser::getType(list);
			if (obj != nullptr)
			{
				listObj->getList().push_back(obj);
				return true;
			}

			return false;
		}

		std::string var = list.substr(0, commaIndex);
		Helper::trim(var);

		Type* obj = Parser::getType(var);
		if (obj == nullptr)
			return false;

		
		listObj->getList().push_back(obj);
		list = list.substr(commaIndex + 1, list.size());
	}

	return true;
}

//asuume that '[' char is exist
std::string Helper::getListFromStr(std::string str)
{
	int indexOpenBracket = str.find("[");
	int openBracketCounter = 1;
	int closeBracketCounter = 0;
	std::string listStr = "";

	int i = 0;
	for (i = indexOpenBracket+1; i < str.size() && openBracketCounter > 0; i++)
	{
		openBracketCounter = str[i] == '[' ? openBracketCounter + 1 : openBracketCounter;
		openBracketCounter = str[i] == ']' ? openBracketCounter - 1 : openBracketCounter;
	}
	
	if (!openBracketCounter)
		return str.substr(indexOpenBracket, i - indexOpenBracket);

	throw SyntaxException();
}

Type* Helper::isVarExist(std::string varName)
{
	std::unordered_map<std::string, Type*>::iterator foundItr = Parser::getVars().find(varName);
	if (foundItr != Parser::getVars().end())
		return foundItr->second;
	return nullptr;
}




bool Helper::isLowerLetter(char c)
{
	return (c >= 'a' && c <= 'z');
}

bool Helper::isUpperLetter(char c)
{
	return (c >= 'A' && c <= 'Z');
}

bool Helper::isLetter(char c)
{
	return (isLowerLetter(c) || isUpperLetter(c));
}

bool Helper::isUnderscore(char c)
{
	return ('_' == c);
}

